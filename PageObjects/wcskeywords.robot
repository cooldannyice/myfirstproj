*** Settings ***
Library             ../utilities/misc.py

*** Variables ***
${INTERSTITIAL_CREATEACCOUNT_BUTTON}        //*[@id='WC_AccountDisplay_links_3']
${INTERSTETIAL_LOGIN_EMAIL_FIELD}           //*[@id='WC_AccountDisplay_FormInput_logonId_In_Logon_1']
${INTERSTETIAL_LOGIN_PASSWORD_FIELD}        //*[@id='WC_AccountDisplay_FormInput_logonPassword_In_Logon_1']
${INTERSTETIAL_LOGIN_SIGNIN_BUTTON}         //*[@id='WC_AccountDisplay_links_2']
${CREATEACCOUNT_FIRSTNAME_FIELD}            //*[@id='WC_UserRegistrationAddForm_FormInput_firstName_In_Register_1']
${CREATEACCOUNT_LASTNAME_FIELD}             //*[@id='WC_UserRegistrationAddForm_FormInput_lastName_In_Register_1']
${CREATEACCOUNT_EMAIL_FIELD}                //*[@id='WC_UserRegistrationAddForm_FormInput_logonId_In_Register_1']
${CREATEACCOUNT_PASSWORD_FIELD}             //*[@id='WC_UserRegistrationAddForm_FormInput_logonPassword_In_Register_1']
${CREATEACCOUNT_CONFIRMPASSWORD_FIELD}      //*[@id='WC_UserRegistrationAddForm_FormInput_logonPasswordVerify_In_Register_1']
${CREATEACCOUNT_ADDRESS1_FIELD}             //*[@id='WC_UserRegistrationAddForm_FormInput_Address1_In_Register_1']
${CREATEACCOUNT_CITY_FIELD}                 //*[@id='WC_UserRegistrationAddForm_FormInput_City_In_Register_1']
${CREATEACCOUNT_STATE_FIELD}                //*[@id='shipping-address-state']
${CREATEACCOUNT_ZIPCODE_FIELD}              //*[@id='WC_UserRegistrationAddForm_FormInput_ZipCode_In_Register_1']
${CREATEACCOUNT_PHONENUMBER_FIELD}          //*[@id='WC_UserRegistrationAddForm_FormInput_phoneNum_In_Register_1']         
${REGISTER_CREATEACCOUNT_BUTTON}            //*[@id='WC_UserRegistrationAddForm_links_1']
${PROFILE_ICON_LOCATOR}                     //*[@class='icon-account']
${PROFILE_SINGOUT_LINK}                     //*[@id='signOutLinkButton']
${MYACC_ADDRESSBOOK_LINK}                   //*[@id='MyAccount_LeftNav_AddressBook_Link_9']
${ADDRESSBOOK_ADDNEWADDRESS_BUTTON}         //*[@id='addAddressButton']
${ADDNEWADDRESS_FIRSTNAME_FIELD}            //*[@id='new-address-firstName']
${ADDNEWADDRESS_LASTNAME_FIELD}             //*[@id='new-address-lastName']
${ADDNEWADDRESS_ADDRESS_FIELD}              //*[@id='new-address-addressLine1']
${ADDNEWADDRESS_CITY_FIELD}                 //*[@id='new-address-city'] 
${ADDNEWADDRESS_STATE_FIELD}                //*[@id='shipping-address-state']
${ADDNEWADDRESS_ZIPCODE_FIELD}              //*[@id='new-address-zipCode']
${ADDNEWADDRESS_PHONE_FIELD}                //*[@id='new-address-phone']
${ADDNEWADDRESS_SET_DEFAULT_CHECKBOX}       //input[@id='primaryAddressCheck']//following-sibling::i
${ADDNEWADDRESS_SAVE_BUTTON}                //button[contains(text(),'save')]
${MYACC_PAYMENTWALLET_LINK}                 //*[@id="MyAccount_LeftNav_PaymentWallet_Link_8"]
${PROFILE_MYACC_BUTTON}                     //a[contains(text(),'My Account')]
${PAYMENTWALLET_ADDNEW_BUTTON}              //a[contains(text(),'add new credit card')]
${NCR_IFRAME}                                             //*[@id="ncrFrame"]//iframe
${NCR_FULLNAME_FIELD}                                     //*[@id="FullName"]
${NCR_ACCOUNTNUMBER_FIELD}                                //*[@id="AccountNumber"]
${NCR_EXP_MONTH_FIELD}                                    //*[@id="ExpirationMonth"]
${NCR_EXP_YEAR_FIELD}                                     //*[@id="ExpirationYear"]
${NCR_CVV}                                                //*[@id='SecurityCode']
${NCR_ADDRESS1_FIELD}                                     //*[@id='NewAddress_AddressLine1']
${NCR_CITY_FIELD}                                         //*[@id='NewAddress_City']
${NCR_STATE_FIELD}                                        //*[@id='NewAddress_StateProvince']
${NCR_ZIPCODE_FIELD}                                      //*[@id='NewAddress_PostalCode']
${NCR_SAVE_BUTTON}                                        //*[@id="SaveButton"]
${NCR_DONE_BUTTON}                                        //*[@id='CompleteNicknameFormButton']
${MYACC_VITAL_CARE_LINK}                                  //a[contains(text(),'Vital Care')]

#User Data
#${USER_FIRSTNAME}                           pcotestmemauto
${USER_LASTNAME}                            test
${USER_PASS}                                petco123
${USER_ADDRESS}                             10850 Via Frontera
${USER_CITY}                                San Diego
${USER_STATE}                               CA
${USER_ZIPCODE}                             92127
${USER_PHONE}                               2624563434
${MEMBER_EMAIL}                             testdot081402@exa.com
${LOGON_URL}                                https://int1-www.petco.com/shop/LogonForm?myAcctMain=1&catalogId=10051&langId=-1&storeId=10151

*** Keywords ***

GENERATE RANDOM NAME AND EMAIL
    ${USER_EMAIL}                                       get_random_email
    Set test variable                                   ${USER_EMAIL}
    ${USER_FIRSTNAME}                                   get_random_username
    Set test variable                                   ${USER_FIRSTNAME}


WCS ACCOUNT REGISTRATION
    wait until page contains element            ${CREATEACCOUNT_FIRSTNAME_FIELD}
    wait until element is visible               ${CREATEACCOUNT_FIRSTNAME_FIELD}
    input text                                  ${CREATEACCOUNT_FIRSTNAME_FIELD}                ${USER_FIRSTNAME}
    input text                                  ${CREATEACCOUNT_LASTNAME_FIELD}                 ${USER_LASTNAME}
    input text                                  ${CREATEACCOUNT_EMAIL_FIELD}                    ${USER_EMAIL}
    input text                                  ${CREATEACCOUNT_PASSWORD_FIELD}                 ${USER_PASS}
    input text                                  ${CREATEACCOUNT_CONFIRMPASSWORD_FIELD}          ${USER_PASS}
    input text                                  ${CREATEACCOUNT_ADDRESS1_FIELD}                 ${USER_ADDRESS}
    input text                                  ${CREATEACCOUNT_CITY_FIELD}                     ${USER_CITY}
    select from list by value                   ${CREATEACCOUNT_STATE_FIELD}                    ${USER_STATE}
    input text                                  ${CREATEACCOUNT_ZIPCODE_FIELD}                  ${USER_ZIPCODE}
    input text                                  ${CREATEACCOUNT_PHONENUMBER_FIELD}              ${USER_PHONE}
    click element                               ${REGISTER_CREATEACCOUNT_BUTTON}
    wait until page does not contain element    ${REGISTER_CREATEACCOUNT_BUTTON}

MYACC ADD NEW ADDRESS
    wait until page contains element            ${PROFILE_ICON_LOCATOR}
    click element                               ${PROFILE_ICON_LOCATOR}
    wait until page contains element            ${MYACC_ADDRESSBOOK_LINK}
    wait until element is visible               ${MYACC_ADDRESSBOOK_LINK}
    click element                               ${MYACC_ADDRESSBOOK_LINK}
    wait until page contains element            ${ADDRESSBOOK_ADDNEWADDRESS_BUTTON}
    click element                               ${ADDRESSBOOK_ADDNEWADDRESS_BUTTON}
    wait until page contains element            ${ADDNEWADDRESS_FIRSTNAME_FIELD}
    input text                                  ${ADDNEWADDRESS_FIRSTNAME_FIELD}                ${USER_FIRSTNAME}
    input text                                  ${ADDNEWADDRESS_LASTNAME_FIELD}                 ${USER_LASTNAME}
    input text                                  ${ADDNEWADDRESS_ADDRESS_FIELD}                  ${USER_ADDRESS}
    input text                                  ${ADDNEWADDRESS_CITY_FIELD}                     ${USER_CITY}
    selectfrom list by value                    ${ADDNEWADDRESS_STATE_FIELD}                    ${USER_STATE}
    input text                                  ${ADDNEWADDRESS_ZIPCODE_FIELD}                  ${USER_ZIPCODE}
    input text                                  ${ADDNEWADDRESS_PHONE_FIELD}                    ${USER_PHONE}
    click element                               ${ADDNEWADDRESS_SET_DEFAULT_CHECKBOX}
    click element                               ${ADDNEWADDRESS_SAVE_BUTTON}
    wait until element is visible               ${ADDRESSBOOK_ADDNEWADDRESS_BUTTON}
    
WCS SIGNIN 
    [Arguments]     ${USER_EMAIL}                                 
    wait until page contains element            ${PROFILE_ICON_LOCATOR}
    click element                               ${PROFILE_ICON_LOCATOR}
    wait until element is visible               ${INTERSTETIAL_LOGIN_EMAIL_FIELD}
    input text                                  ${INTERSTETIAL_LOGIN_EMAIL_FIELD}               ${USER_EMAIL}
    click element                               ${INTERSTETIAL_LOGIN_PASSWORD_FIELD}
    input text                                  ${INTERSTETIAL_LOGIN_PASSWORD_FIELD}            ${USER_PASS}
    click element                               ${INTERSTETIAL_LOGIN_SIGNIN_BUTTON}
    wait until page does not contain element    ${INTERSTETIAL_LOGIN_EMAIL_FIELD}

WCS SIGNOUT
    mouse over                                  ${PROFILE_ICON_LOCATOR}
    wait until page contains element            ${PROFILE_SINGOUT_LINK}
    click element                               ${PROFILE_SINGOUT_LINK} 

MYACC ADD NEW PAYMENT
    [Arguments]     ${CC_ACCOUNT}       ${CC_BILLING_ADDRESS}       ${CC_BILLING_CITY}      ${CC_BILLING_STATE}     ${CC_BILLING_ZIPCODE} 
    NAVIGATE TO PAYMENT WALLET PAGE
    INITIATE ADD NEW PAYMENT
    wcskeywords.Enter Credit card info          ${CC_ACCOUNT}         
    wcskeywords.Enter Billing address           ${CC_BILLING_ADDRESS}       ${CC_BILLING_CITY}      ${CC_BILLING_STATE}     ${CC_BILLING_ZIPCODE} 
    wcskeywords.Submit Credit card

NAVIGATE TO PAYMENT WALLET PAGE 
    wait until page contains element            ${PROFILE_ICON_LOCATOR}
    click element                               ${PROFILE_ICON_LOCATOR}
    wait until page contains element            ${MYACC_PAYMENTWALLET_LINK}
    wait until element is visible               ${MYACC_PAYMENTWALLET_LINK}
    click element                               ${MYACC_PAYMENTWALLET_LINK}
    wait until page contains element            ${PAYMENTWALLET_ADDNEW_BUTTON}

INITIATE ADD NEW PAYMENT
    wait until page contains element            ${PAYMENTWALLET_ADDNEW_BUTTON}
    click element                               ${PAYMENTWALLET_ADDNEW_BUTTON}
    wait until page contains element            ${NCR_IFRAME}

Enter Credit card info
    [Arguments]     ${CC_ACCOUNT}  
    wait until page contains element                    ${NCR_IFRAME}
    Select frame                                        ${NCR_IFRAME}
    wait until page contains element                    ${CC_FULLNAME_FIELD}
    input text                                          ${CC_FULLNAME_FIELD}                    ${CC_FULLNAME}
    input text                                          ${CC_ACCOUNTNUMBER_FIELD}               ${CC_ACCOUNT}
    select from list by value                           ${CC_EXP_MONTH_FIELD}                   ${CC_EXP_MONTH}
    select from list by value                           ${CC_EXP_YEAR_FIELD}                    ${CC_EXP_YEAR}
    input text                                          ${NCR_CVV}                              ${CC_CVV}

Enter Billing address
    [Arguments]     ${CC_BILLING_ADDRESS}       ${CC_BILLING_CITY}      ${CC_BILLING_STATE}     ${CC_BILLING_ZIPCODE}     
    input text                                          ${CC_ADDRESS1_FIELD}                    ${CC_BILLING_ADDRESS}
    input text                                          ${CC_CITY_FIELD}                        ${CC_BILLING_CITY}
    Select from list by value                           ${CC_STATE_FIELD}                       ${CC_BILLING_STATE}
    input text                                          ${CC_ZIPCODE_FIELD}                     ${CC_BILLING_ZIPCODE}

Submit Credit card
    click element                                       ${CC_SAVE_BUTTON}
    wait until page contains element                    ${CC_DONE_BUTTON}
    wait until page contains element                    ${CC_DONE_BUTTON}
    wait until element is visible                       ${CC_DONE_BUTTON}
    click element                                       ${CC_DONE_BUTTON}
    wait until element is visible                       ${PAYMENTWALLET_ADDNEW_BUTTON}      
    Unselect Frame

INTERSTITIAL PAGE --> LOGIN
    wait until page contains element            ${INTERSTETIAL_LOGIN_EMAIL_FIELD}
    wait until element is visible               ${INTERSTETIAL_LOGIN_EMAIL_FIELD}
    input text                                  ${INTERSTETIAL_LOGIN_EMAIL_FIELD}               ${USER_EMAIL}
    click element                               ${INTERSTETIAL_LOGIN_PASSWORD_FIELD}
    input text                                  ${INTERSTETIAL_LOGIN_PASSWORD_FIELD}            ${USER_PASS}
    click element                               ${INTERSTETIAL_LOGIN_SIGNIN_BUTTON}
    wait until page does not contain element    ${INTERSTETIAL_LOGIN_EMAIL_FIELD}

NAVIGATE TO MYACC PAGE
    wait until page contains element            ${PROFILE_ICON_LOCATOR}
    mouse over                                  ${PROFILE_ICON_LOCATOR}
    wait until element is visible               ${PROFILE_MYACC_BUTTON}
    click element                               ${PROFILE_MYACC_BUTTON}
    WAIT FOR LOADER
    wait until page contains element            ${MYACC_ADDRESSBOOK_LINK}

SETUP A WCS USER WITH ADDRESS AND PAYMENT
    WCS SIGNIN                                  ${USER_EMAIL}
    NAVIGATE TO MYACC PAGE
    MYACC ADD NEW PAYMENT  ${VISA_CARD}  ${CC_BILLING_ADDRESS}  ${CC_BILLING_CITY}  ${CC_BILLING_STATE}  ${CC_BILLING_ZIPCODE}
    MYACC ADD NEW ADDRESS
    WCS SIGNOUT

NAVIGATE TO MYCORE CARE
    NAVIGATE TO MYACC PAGE
    wait until page contains element            ${MYACC_VITAL_CARE_LINK}  
    wait until element is visible               ${MYACC_VITAL_CARE_LINK}  
    click element                               ${MYACC_VITAL_CARE_LINK}
    WAIT FOR LOADER