*** Variables ***
${MEM_DASH_REDEEM_LINK}                              //*[@name='redeemBenefits']
${MEM_DASH_MEM_DETAILS_LINK}                         //*[@name="viewMembershipDetails"]
${MEM_DASH_ADDAPET_LINK}                             //*[@name="addNewPet"]
${MEM_DASH_BENEFITUSAGE_LINK}                        //*[@name="benefitUsageHistory"]
${MEM_BENEFIT_VACCINATIONCOUPON_VIEW_BUTTON}         //h4[contains(text(),'Vaccination')]//parent::div//parent::div//following-sibling::div//button
${MEM_COUPON_MODAL_REDEEM_BUTTON}                    //span[contains(text(),'Redeem now')]//parent::button
${MEM_COUPON_MODAL_DONE_BUTTON}                      //span[contains(text(),'Done with coupon')]//parent::button
${MEM_DETAILS_AUTORENEW_STOP_LINK}                   //span[contains(text(),'Stop auto-renew')]//parent::button
${MEM_DETAILS_AUTORENEW_START_LINK}                  //span[contains(text(),'Start auto-renew')]//parent::button
${MEM_AUTORENEW_MODAL_YES}                           //span[contains(text(),'Yes')]//parent::button  
${MEM_BENEFITUSAGE_CONGRATS_MESSAGE}                 //h5[contains(text(),'Congrats On Your Redemptions!')]


*** Keywords ***
MEMBERSHIP DASHBOARD --> DISPLAY                                  
    wait until page contains element            //*[@class="registration-container"]//li[@data-testid='${PET_NAME}']
    click element                               //*[@class="registration-container"]//li[@data-testid='${PET_NAME}']
    wait until page contains element            //*[@class="registration-container"]//h1
    ${ACTUAL_HEADING}     Get Text              //*[@class="registration-container"]//h1
    Should be equal as strings                  ${ACTUAL_HEADING}      Welcome back, ${USER_FIRSTNAME}!         
    wait until page contains element            ${MEM_DASH_REDEEM_LINK}
    wait until page contains element            ${MEM_DASH_MEM_DETAILS_LINK}
    wait until page contains element            ${MEM_DASH_ADDAPET_LINK}
    wait until page contains element            ${MEM_DASH_BENEFITUSAGE_LINK}

MEMBERSHIP DASHBOARD --> BENEFIT REDEMPTION SCREEN DB COUPON
    click element                               ${MEM_DASH_REDEEM_LINK}
    Wait for loader                             
    #Press Keys                                  //*[@class="registration-container"]//li[@data-testid='${PET_NAME}']             PAGE_DOWN
    wait until element is visible               ${MEM_BENEFIT_VACCINATIONCOUPON_VIEW_BUTTON}
    click element                               ${MEM_BENEFIT_VACCINATIONCOUPON_VIEW_BUTTON}
    Wait for loader

MEMBERSHIP DASHBOARD --> COUPON REDEEM MODAL
    wait until page contains element            ${MEM_COUPON_MODAL_REDEEM_BUTTON}
    wait until element is visible               ${MEM_COUPON_MODAL_REDEEM_BUTTON}  
    click element                               ${MEM_COUPON_MODAL_REDEEM_BUTTON}
    wait until page contains element            ${MEM_COUPON_MODAL_DONE_BUTTON}
    wait until element is visible               ${MEM_COUPON_MODAL_DONE_BUTTON}
    click element                               ${MEM_COUPON_MODAL_DONE_BUTTON}
    wait until element is not visible           ${MEM_COUPON_MODAL_DONE_BUTTON}

GO BACK TO MEMBERSHIP DASHBOARD
    [Documentation]     This is only until we have an actual back button in dashboard pages
    Go back
    wait for loader
    Select Frame                                ${MEMBERSHIP_IFRAME}
    wait until element is visible               ${MEM_DASH_REDEEM_LINK} 

MEMBERSHIP DASHBOARD --> MEMBERSHIP DETAILS SCREEN
    wait until page contains element            ${MEM_DASH_MEM_DETAILS_LINK}
    click element                               ${MEM_DASH_MEM_DETAILS_LINK}
    Wait for loader
    wait until page contains element            ${MEM_DETAILS_AUTORENEW_STOP_LINK}
    click element                               ${MEM_DETAILS_AUTORENEW_STOP_LINK}
    wait until page contains element            ${MEM_AUTORENEW_MODAL_YES}
    wait until element is visible               ${MEM_AUTORENEW_MODAL_YES}
    click element                               ${MEM_AUTORENEW_MODAL_YES}
    Wait for loader
    wait until page contains element            ${MEM_DETAILS_AUTORENEW_START_LINK}
    wait until element is visible               ${MEM_DETAILS_AUTORENEW_START_LINK}

MEMBERSHIP DASHBOARD --> BENEFIT USAGE SCREEN
    wait until page contains element            ${MEM_DASH_BENEFITUSAGE_LINK}
    click element                               ${MEM_DASH_BENEFITUSAGE_LINK}
    Wait for loader
    wait until page contains element            ${MEM_BENEFITUSAGE_CONGRATS_MESSAGE}                             

MEMBERSHIP DASHBOARD --> ADD A PET FLOW INITIATE
    wait until page contains element            ${MEM_DASH_ADDAPET_LINK}
    click element                               ${MEM_DASH_ADDAPET_LINK}
    Wait for loader
    wait until page contains element            ${MEM_ADDAPET_BUTTON}
             