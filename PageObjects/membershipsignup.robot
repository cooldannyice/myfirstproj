*** Settings ***
Library             ../utilities/misc.py

*** Variables ***
${MEMBERSHIP_IFRAME}                                     //*[@id='membership-iframe']
${PETCO_LOADER}                                          //*[@id='petco-loader']
${MEM_SIGNUP_SELECTPETS_BUTTON}                          //span[contains(text(),'Select Pets')]//parent::button
${MEM_ADDAPET_BUTTON}                                    //*[@name='addNewPet']
${MEM_PETNAME_FIELD}                                     //*[@data-testid='petName']
${MEM_NEXT_BUTTON}                                       //*[@data-testid='next-button']
${MEM_PET_TYPE_DOG_BUTTON}                               //span[contains(text(),'Dog')]//parent::button
${MEM_PET_TYPE_CAT_BUTTON}                               //span[contains(text(),'Cat')]//parent::button
${MEM_SELECT_BREED_FIELD}                                //div[contains(text(),'Select Breed')]//parent::div
${MEM_SELECT_BREED_INPUT}                                //*[@id='breed']//input[starts-with(@id,'react-select')]
${MEM_SELECT_BREED_VALUE}                                //div[starts-with(@id,'react-select')]
${MEM_SELECT_DOB_MONTH_FIELD}                            //div[contains(text(),'Select Month')]//parent::div
${MEM_SELECT_DOB_MONTH_VALUE}                            //div[starts-with(@id,'react-select')]
${MEM_SELECT_DOB_MONTH_INPUT}                            //*[@id='month']//input[starts-with(@id,'react-select')]
${MEM_SELECT_DOB_YEAR_FIELD}                             //div[contains(text(),'Select Year')]//parent::div
${MEM_SELECT_DOB_YEAR_VALUE}                             //div[starts-with(@id,'react-select')]
${MEM_SELECT_DOB_YEAR_INPUT}                             //*[@id='year']//input[starts-with(@id,'react-select')]
${MEM_PET_ZIPCODE_FIELD}                                 //input[@name='zipCode']
${MEM_CREATE_PET_BUTTON}                                 //button[@id='next-button']
${MEM_SELECT_PET_FOR_MEMBERSHIP}                         //*[@id='choose-add-ons-button']
${MEM_ENTER_YOUR_ADDRESS_BUTTON}                         //span[contains(text(),'Enter Your Address')]//parent::button
${MEM_ADDRESS_FIRSTNAME}                                 //*[@name='firstName']
${MEM_ADDRESS_LASTNAME}                                  //*[@name='lastName']
${MEM_ADDRESS_ADDRESS1}                                  //*[@name='address']
${MEM_ADDRESS_CITY}                                      //*[@name='city']
${MEM_ADDRESS_ZIP}                                       //*[@name='zip']
${MEM_ENTER_YOUR_PAYMENT_BUTTON}                         //span[contains(text(),'Enter Payment Information')]//parent::button
${MEM_SELECT_PAYMENT_DROPDOWN}                           //*[@id='paymentMethod']
${MEM_SELECT_ADD_NEW_CARD}                               //div[contains(text(),'Add new payment method')]
${MEM_SELECT_PAYMENT_BUTTON}                             //span[contains(text(),'Select Payment')]//parent::button
${MEM_TAX_VALUE}                                         //*[@data-testid='purchase-total']
${MEM_SUBMIT_PAYMENT_BUTTON}                             //span[contains(text(),'Submit Payment')]//parent::button
${CC_IFRAME}                                             //iframe[@id='add-card-iframe']
${CC_FULLNAME_FIELD}                                     //*[@id="FullName"]
${CC_ACCOUNTNUMBER_FIELD}                                //*[@id="AccountNumber"]
${CC_EXP_MONTH_FIELD}                                    //*[@id="ExpirationMonth"]
${CC_EXP_YEAR_FIELD}                                     //*[@id="ExpirationYear"]
${CC_ADDRESS1_FIELD}                                     //*[@id='NewAddress_AddressLine1']
${CC_CITY_FIELD}                                         //*[@id='NewAddress_City']
${CC_STATE_FIELD}                                        //*[@id='NewAddress_StateProvince']
${CC_ZIPCODE_FIELD}                                      //*[@id='NewAddress_PostalCode']
${CC_SAVE_BUTTON}                                        //*[@id="SaveButton"]
${CC_DONE_BUTTON}                                        //*[@id='CompleteNicknameFormButton']
${MEM_PROCCED_DASHBOARD_BUTTON}                          //*[@data-testid='return-to-dashboard-button']
${MEM_T&C_CHECKBOX}                                      //*[@class="terms-checkbox-row-section"]//label
${MEM_CA_TAX_VALUE}                                            $0
${MEM_NY_TAX_VALUE}                                            $1.69
${MEM_EDIT_PAYMENT_INFO_LINK}                            //h1[contains(text(),'Payment Information')]//following-sibling::span

#Pet Data
${PET_BREED}                                             German Hound
${PET_DOB_MONTH}                                         March
${PET_DOB_YEAR}                                          2017
${PET_ZIPCODE}                                           92127
#CC Data
${CC_FULLNAME}                                           Test card
${VISA_CARD}                                             4111111111111111
${DISCOVER_CARD}                                         6011361000002220
${AMEX_CARD}                                             379605170000995
${CC_EXP_MONTH}                                          12
${CC_EXP_YEAR}                                           2024
${CC_CVV}                                                234
${CC_BILLING_ADDRESS}                                    10850 Via Frontera
${CC_BILLING_CITY}                                       San Diego
${CC_BILLING_STATE}                                      CA
${CC_BILLING_ZIPCODE}                                    92127
${CC_BILLING_ADDRESS_2}                                  1555 Linden Blvd
${CC_BILLING_CITY_2}                                     Brooklyn
${CC_BILLING_STATE_2}                                    NY
${CC_BILLING_ZIPCODE_2}                                  11212



*** Keywords ***
WAIT FOR LOADER
    wait until element is not visible                   ${PETCO_LOADER} 

BENEFIT PLAN PAGE --> SELECT PETS
    wait until page contains element                    ${MEMBERSHIP_IFRAME}
    Select Frame                                        ${MEMBERSHIP_IFRAME}
    wait until page contains element                    ${MEM_SIGNUP_SELECTPETS_BUTTON}
    wait until element is visible                       ${MEM_SIGNUP_SELECTPETS_BUTTON}
    click element                                       ${MEM_SIGNUP_SELECTPETS_BUTTON}
    Wait for loader                      

PET SELECTION SCREEN --> ADDNEW PET
    ${PET_NAME}     get_random_petname
    Set global Variable                                 ${PET_NAME}
    wait until page contains element                    ${MEM_ADDAPET_BUTTON}
    wait until element is visible                       ${MEM_ADDAPET_BUTTON}
    click element                                       ${MEM_ADDAPET_BUTTON}
    wait until page contains element                    ${MEM_PETNAME_FIELD}
    wait until element is visible                       ${MEM_PETNAME_FIELD}
    input text                                          ${MEM_PETNAME_FIELD}                ${PET_NAME}
    wait until element is enabled                       ${MEM_NEXT_BUTTON}
    click element                                       ${MEM_NEXT_BUTTON}
    wait until page contains element                    ${MEM_PET_TYPE_DOG_BUTTON}
    wait until element is visible                       ${MEM_PET_TYPE_DOG_BUTTON}
    click element                                       ${MEM_PET_TYPE_DOG_BUTTON}
    wait until page contains element                    ${MEM_SELECT_BREED_FIELD}
    wait until element is visible                       ${MEM_SELECT_BREED_FIELD}
    click element                                       ${MEM_SELECT_BREED_FIELD}
    input text                                          ${MEM_SELECT_BREED_INPUT}           ${PET_BREED}
    wait until element is visible                       ${MEM_SELECT_BREED_VALUE}
    click element                                       ${MEM_SELECT_BREED_VALUE}
    wait until page contains element                    ${MEM_SELECT_DOB_MONTH_FIELD}
    wait until element is visible                       ${MEM_SELECT_DOB_MONTH_FIELD}
    click element                                       ${MEM_SELECT_DOB_MONTH_FIELD}
    input text                                          ${MEM_SELECT_DOB_MONTH_INPUT}       ${PET_DOB_MONTH}
    wait until element is visible                       ${MEM_SELECT_DOB_MONTH_VALUE}
    click element                                       ${MEM_SELECT_DOB_MONTH_VALUE}
    wait until page contains element                    ${MEM_SELECT_DOB_YEAR_FIELD}
    wait until element is visible                       ${MEM_SELECT_DOB_YEAR_FIELD}
    click element                                       ${MEM_SELECT_DOB_YEAR_FIELD}
    input text                                          ${MEM_SELECT_DOB_YEAR_INPUT}        ${PET_DOB_YEAR}
    wait until element is visible                       ${MEM_SELECT_DOB_YEAR_VALUE}
    click element                                       ${MEM_SELECT_DOB_YEAR_VALUE}
    wait until page contains element                    ${MEM_PET_ZIPCODE_FIELD}
    wait until element is visible                       ${MEM_PET_ZIPCODE_FIELD}
    click element                                       ${MEM_PET_ZIPCODE_FIELD}
    input text                                          ${MEM_PET_ZIPCODE_FIELD}            ${PET_ZIPCODE}
    wait until element is enabled                       ${MEM_CREATE_PET_BUTTON}
    click element                                       ${MEM_CREATE_PET_BUTTON}
    wait until page does not contain element            ${MEM_PET_ZIPCODE_FIELD} 

PET SELECTION SCREEN --> SELECT PET
    wait until page contains element                    ${MEM_SELECT_PET_FOR_MEMBERSHIP}
    wait until element is visible                       ${MEM_SELECT_PET_FOR_MEMBERSHIP}
    click element                                       ${MEM_SELECT_PET_FOR_MEMBERSHIP}

COMPLETE PURCHASE --> PROCEED FROM REVIEW
    wait until page contains element                    ${MEM_ENTER_YOUR_ADDRESS_BUTTON}
    wait until element is visible                       ${MEM_ENTER_YOUR_ADDRESS_BUTTON}
    click element                                       ${MEM_ENTER_YOUR_ADDRESS_BUTTON}   

COMPLETE PURCHASE --> PROCEED FROM ADDRESS
    wait until page contains element                    ${MEM_ENTER_YOUR_PAYMENT_BUTTON}
    #Press Keys                                          ${MEM_ADDRESS_FIRSTNAME}             PAGE_DOWN
    wait until element is visible                       ${MEM_ENTER_YOUR_PAYMENT_BUTTON}
    click element                                       ${MEM_ENTER_YOUR_PAYMENT_BUTTON}
    

COMPLETE PURCHASE --> ADD NEW PAYMENT
    [Arguments]     ${CARD}     ${CC_BILLING_ADDRESS}   ${CC_BILLING_CITY}      ${CC_BILLING_STATE}     ${CC_BILLING_ZIPCODE}
    wait until page contains element                    ${MEM_SELECT_PAYMENT_DROPDOWN}
    wait until element is visible                       ${MEM_SELECT_PAYMENT_DROPDOWN}
    click element                                       ${MEM_SELECT_PAYMENT_DROPDOWN}
    wait until element is visible                       ${MEM_SELECT_ADD_NEW_CARD}
    click element                                       ${MEM_SELECT_ADD_NEW_CARD}
    membershipsignup.Enter Credit card info             ${CARD}       
    membershipsignup.Enter Billing address              ${CC_BILLING_ADDRESS}   ${CC_BILLING_CITY}      ${CC_BILLING_STATE}     ${CC_BILLING_ZIPCODE}
    membershipsignup.Submit Credit card


Enter Credit card info
    [Arguments]         ${CARD}
    wait until page contains element                    ${CC_IFRAME}
    Select frame                                        ${CC_IFRAME}
    wait until page contains element                    ${CC_FULLNAME_FIELD}
    input text                                          ${CC_FULLNAME_FIELD}                    ${CC_FULLNAME}
    input text                                          ${CC_ACCOUNTNUMBER_FIELD}               ${CARD}
    select from list by value                           ${CC_EXP_MONTH_FIELD}                   ${CC_EXP_MONTH}
    select from list by value                           ${CC_EXP_YEAR_FIELD}                    ${CC_EXP_YEAR}

Enter Billing address
    [Arguments]         ${CC_BILLING_ADDRESS}   ${CC_BILLING_CITY}      ${CC_BILLING_STATE}     ${CC_BILLING_ZIPCODE}
    input text                                          ${CC_ADDRESS1_FIELD}                    ${CC_BILLING_ADDRESS}
    input text                                          ${CC_CITY_FIELD}                        ${CC_BILLING_CITY}
    Select from list by value                           ${CC_STATE_FIELD}                       ${CC_BILLING_STATE}
    input text                                          ${CC_ZIPCODE_FIELD}                     ${CC_BILLING_ZIPCODE}

Submit Credit card
    [Documentation]     Adding Press Keys because the NCR iframe has lot of white space , this is a known issue and cannot be fixed
    click element                                       ${CC_SAVE_BUTTON}
    wait until page contains element                    ${CC_DONE_BUTTON}
    Unselect Frame
    Press Keys                                          //*[@class='icon-search persistent-search']                PAGE_UP
    Select frame                                        ${MEMBERSHIP_IFRAME}
    Select frame                                        ${CC_IFRAME}
    wait until page contains element                    ${CC_DONE_BUTTON}
    click element                                       ${CC_DONE_BUTTON}
    Unselect Frame


COMPLETE PURCHASE --> PROCEED FROM PAYMENT
    Select Frame                                        ${MEMBERSHIP_IFRAME}
    Wait for loader
    wait until page contains element                    ${MEM_SELECT_PAYMENT_BUTTON}
    wait until element is visible                       ${MEM_SELECT_PAYMENT_BUTTON}
    click element                                       ${MEM_SELECT_PAYMENT_BUTTON}

COMPLETE PURCHASE --> SUBMIT PAYMENT
    wait for loader
    wait until page contains element                    ${MEM_T&C_CHECKBOX}
    click element                                       ${MEM_T&C_CHECKBOX}
    wait until page contains element                    ${MEM_SUBMIT_PAYMENT_BUTTON}
    wait until element is visible                       ${MEM_SUBMIT_PAYMENT_BUTTON}
    click element                                       ${MEM_SUBMIT_PAYMENT_BUTTON}
    wait until page does not contain element            ${MEM_SUBMIT_PAYMENT_BUTTON}


PURCHASE SUCCESS --> PROCEED TO DASHBOARD 
    wait until page contains element                    ${MEM_PROCCED_DASHBOARD_BUTTON}
    click element                                       ${MEM_PROCCED_DASHBOARD_BUTTON}
    wait until page does not contain element            ${MEM_PROCCED_DASHBOARD_BUTTON}



LOGIN FROM INTERSTETIAL PAGE
    

INTERSTITIAL PAGE --> CREATE ACCOUNT
    wait until page contains element            ${INTERSTITIAL_CREATEACCOUNT_BUTTON}
    click element                               ${INTERSTITIAL_CREATEACCOUNT_BUTTON}

PURCHASE MEMBERSHIP FROM LP --> NEW USER --> ONE NEW PET
    INTERSTITIAL PAGE --> CREATE ACCOUNT
    WCS ACCOUNT REGISTRATION
    BENEFIT PLAN PAGE --> SELECT PETS
    PET SELECTION SCREEN --> ADDNEW PET
    WAIT FOR LOADER
    PET SELECTION SCREEN --> SELECT PET 
    WAIT FOR LOADER
    COMPLETE PURCHASE --> PROCEED FROM REVIEW
    COMPLETE PURCHASE --> PROCEED FROM ADDRESS
    COMPLETE PURCHASE --> ADD NEW PAYMENT       ${VISA_CARD}     ${CC_BILLING_ADDRESS}   ${CC_BILLING_CITY}      ${CC_BILLING_STATE}     ${CC_BILLING_ZIPCODE}
    COMPLETE PURCHASE --> PROCEED FROM PAYMENT
    COMPLETE PURCHASE --> SUBMIT PAYMENT
    PURCHASE SUCCESS --> PROCEED TO DASHBOARD

GO TO MEMBERSHIP APP 
    GO TO                           ${URL}

COMPLETE PURCHASE --> VERIFY TAX
    [Arguments]             ${EXPTECTED_TAX}
    wait until page contains element            ${MEM_TAX_VALUE}
    ${ACTUAL_TAX}       Get text                ${MEM_TAX_VALUE}
    should be equal as strings      ${ACTUAL_TAX}       ${EXPTECTED_TAX}

COMPLETE PURCHASE --> EDIT PAYMENT
    wait until page contains element            ${MEM_EDIT_PAYMENT_INFO_LINK}
    wait until element is visible               ${MEM_EDIT_PAYMENT_INFO_LINK}
    click element                               ${MEM_EDIT_PAYMENT_INFO_LINK}
    wait until element is not visible           ${MEM_EDIT_PAYMENT_INFO_LINK}
    

COMPLETE PURCHASE --> VALIDATE EXISTING ADDRESS
    wait until element is visible               ${MEM_ADDRESS_FIRSTNAME}
    Element attribute value should be           ${MEM_ADDRESS_FIRSTNAME}      value   ${USER_FIRSTNAME}
    Element attribute value should be           ${MEM_ADDRESS_LASTNAME}       value   ${USER_LASTNAME}
    Element attribute value should be           ${MEM_ADDRESS_ADDRESS1}       value   ${USER_ADDRESS}
    Element attribute value should be           ${MEM_ADDRESS_CITY}           value   ${USER_CITY}
    Element attribute value should be           ${MEM_ADDRESS_ZIP}            value   ${USER_ZIPCODE}
    
COMPLETE PURCHASE --> VALIDATE EXISTING PAYMENT METHOD
    wait until page contains element                    ${MEM_SELECT_PAYMENT_DROPDOWN}
    wait until element is visible                       ${MEM_SELECT_PAYMENT_DROPDOWN}
    click element                                       ${MEM_SELECT_PAYMENT_DROPDOWN}
    wait until element is visible                       ${MEM_SELECT_ADD_NEW_CARD}
    wait until page contains                            Visa ending in 1111

             