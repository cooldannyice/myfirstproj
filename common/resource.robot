*** Settings ***
Library             ../utilities/sauce.py

*** Variables ***
${URL}                      https://int1-www.petco.com/shop/PetcoCoreCareView?langId=-1&langId=-1&catalogId=10051&storeId=10151&redirectBack=true
#${URL}                      https://qa.petcoach.co
${browserName}              chrome
${platform}                 Windows 10
${version}                  latest
${SAUCE_USERNAME}           username
${SAUCE_ACCESS_KEY}         accesskey
${REMOTE_URL}               https://${SAUCE_USERNAME}:${SAUCE_ACCESS_KEY}@ondemand.saucelabs.com:443/wd/hub
${tunnelidentifier}         PetcoVM
${BUILD}                    Membership
${TAG}                      Membership
${capabilities}             tags:${TAG},browserName:${browserName},platform:${platform},version:${version},name:${SUITE_NAME},tunnelidentifier:${tunnelidentifier},build:${BUILD}    


*** Keywords ***
Update Status
    ${sauce_job_id}             Get Session Id
    ${Sauce_test_status}        Set Variable If      '${TEST STATUS}'=='PASS'    ${TRUE}    ${FALSE}
    sauce.update_test_status    ${sauce_job_id}     ${sauce_test_status}    ${SAUCE_USERNAME}   ${SAUCE_ACCESS_KEY}

Open App
    Open browser        ${URL}      remote_url=${REMOTE_URL}    desired_capabilities=${capabilities}

Open App Local
    Open browser        ${URL}      chrome
    Maximize browser window

Open App Local headless
    Open browser        ${URL}      headlesschrome
    Maximize browser window

